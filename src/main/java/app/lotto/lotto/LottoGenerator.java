package app.lotto.lotto;

import app.lotto.lotto.model.entity.LottoResultsData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class LottoGenerator {

    private final LottoDataManager lottoDataManager;

    @Autowired
    public LottoGenerator(LottoDataManager lottoDataConfig) {
        this.lottoDataManager = lottoDataConfig;
        this.lottoDataManager.prepareData();
    }

    public List<List<Integer>> generateLottoNumbers(int iterationsCount, LottoGameConfig gameConfig) {
        List<List<Integer>> numbersBasket = new ArrayList<>();
        for (int iteration = 0; iteration < iterationsCount; iteration++) {
            List<Integer> numbers = new ArrayList<>();
            do {
                for (int i = 0, number; i < gameConfig.getCount(); i++) {
                    do {
                        number = new Random().nextInt(gameConfig.getBound()) + 1;
                    }
                    while (numbers.contains(number));
                    numbers.add(number);
                }
            }
            while (isNumbersExists(lottoDataManager.getLottoResultsList(), numbers));
            numbersBasket.add(numbers);
        }
        return numbersBasket;
    }

    public boolean isNumbersExists(List<LottoResultsData> lottoResultsList, List<Integer> numbers) {
        String numbersInString = numbers.stream()
                .sorted(Comparator.naturalOrder())
                .map(String::valueOf)
                .collect(Collectors.joining(", "));

        boolean isNumbersExists = lottoResultsList.stream()
            .map(LottoResultsData::getNumbers)
            .anyMatch(number -> number.equals(numbersInString)) ;

        if (isNumbersExists) {
            System.out.println("NumbersExists: " + numbersInString);
        }
        return isNumbersExists;
    }
}
