package app.lotto.lotto;

import app.lotto.lotto.model.entity.LottoResultsData;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.time.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Component
@PropertySource("classpath:application.properties")
public class LottoDataManager {

    private final List<LottoResultsData> lottoResultsList = new ArrayList<>();
    private final List<LottoResultsData> miniLottoResultsList = new ArrayList<>();

    @Value("${lotto.file.path}")
    public String filesPath;

    @Value("${lotto.file.path.big}")
    public String bigLottoFilePath;

    @Value("${lotto.file.path.mini}")
    public String miniLottoFilePath;

    Logger logger = LogManager.getLogger(LottoDataManager.class);

    public void prepareData() {
        prepareBigLottoData();
        prepareMiniLottoData();
    }

    public void prepareBigLottoData() {
        readFile(filesPath + bigLottoFilePath, LottoGameConfig.BIG);
        ZonedDateTime nowInPoland = ZonedDateTime.now(ZoneId.of("Europe/Warsaw"));
        LocalDateTime lastLottoResultsDateTime = nowInPoland.toLocalDateTime()
                .withHour(LottoResultsData.lottoResultsTime.getHour())
                .withMinute(LottoResultsData.lottoResultsTime.getMinute())
                .withSecond(LottoResultsData.lottoResultsTime.getSecond())
                .withNano(LottoResultsData.lottoResultsTime.getNano());

        do {
            lastLottoResultsDateTime = lastLottoResultsDateTime.minusDays(1);
        }
        while(!LottoResultsData.bigLottoResultsDays.contains(lastLottoResultsDateTime.getDayOfWeek()));

        if (lottoResultsList.isEmpty() || resultListIsNotUpToDate(lastLottoResultsDateTime)) {
            downloadLottoResultsFile();
            readFile(filesPath + bigLottoFilePath, LottoGameConfig.BIG);
        }
        //System.out.println("last lotto result date time: " + lastLottoResultsDateTime.toString());
        logger.log(Level.INFO, "Last Lotto Results Date:  " + lastLottoResultsDateTime.toString());
    }

    public void prepareMiniLottoData() {
        readFile(filesPath + miniLottoFilePath, LottoGameConfig.MINI);
    }

    private boolean resultListIsNotUpToDate(LocalDateTime lastLottoResultsDateTime) {
        return lottoResultsList.get(lottoResultsList.size() - 1).getDate().isBefore(lastLottoResultsDateTime);
    }

    public void downloadLottoResultsFile() {
        //duze lotto
        //http://www.mbnet.com.pl/dl.txt

        //mini lotto
        //http://www.mbnet.com.pl/el.txt
    }

    public void readFile(String filePath, LottoGameConfig gameConfig) {
        try (BufferedReader reader = Files.newBufferedReader(ResourceUtils.getFile(filePath).toPath())) {
            switch (gameConfig) {
                case BIG -> reader.lines().forEach(line -> lottoResultsList.add(new LottoResultsData(line)));
                case MINI -> reader.lines().forEach(line -> miniLottoResultsList.add(new LottoResultsData(line)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
