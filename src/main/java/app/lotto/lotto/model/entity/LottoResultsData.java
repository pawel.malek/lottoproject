package app.lotto.lotto.model.entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class LottoResultsData implements Serializable {

    public final static String DATE_PATTERN = "dd.MM.yyyy";
    public final static String TIME_PATTERN = "HH:mm:ss:nn";
    public static final LocalTime lottoResultsTime = LocalTime.parse("21:50:01:01", DateTimeFormatter.ofPattern(TIME_PATTERN));
    public static final List<DayOfWeek> bigLottoResultsDays = Arrays.asList(DayOfWeek.TUESDAY, DayOfWeek.THURSDAY, DayOfWeek.SATURDAY);

    @Id
    Long id;
    LocalDateTime date;
    String numbers;

    public LottoResultsData(String dataLine) {
        String[] data = dataLine.split(" ");
        this.id = Long.valueOf(data[0].replace(".", ""));
        this.date = LocalDateTime.of(LocalDate.parse(data[1], DateTimeFormatter.ofPattern(DATE_PATTERN)),
                LocalTime.of(lottoResultsTime.getHour(), lottoResultsTime.getMinute(), lottoResultsTime.getSecond(), lottoResultsTime.getNano())
        );

        this.numbers = Arrays.stream(data[2].split(","))
                .map(Integer::valueOf)
                .sorted(Comparator.naturalOrder())
                .map(String::valueOf)
                .collect(Collectors.joining(", "));;
    }

}
