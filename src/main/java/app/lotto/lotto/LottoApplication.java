package app.lotto.lotto;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;

import java.util.*;
import java.util.stream.Collectors;

//@SpringBootApplication
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class LottoApplication {

    LottoGenerator generator;

    Logger logger = LogManager.getLogger(LottoApplication.class);

    @Autowired
    public LottoApplication(LottoGenerator generator) {
        this.generator = generator;
    }

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(LottoApplication.class);
        app.setBannerMode(Banner.Mode.LOG);
        app.run(args);
       // SpringApplication.run(LottoApplication.class, args);
    }

    @Bean
    public CommandLineRunner run() {
        return args -> {
           List<List<Integer>> numbers = generator.generateLottoNumbers(10, LottoGameConfig.BIG);
            printResults(LottoGameConfig.BIG.name(), numbers);

            numbers = generator.generateLottoNumbers(10, LottoGameConfig.MINI);
            printResults(LottoGameConfig.MINI.name(), numbers);
        };
    }

    public void printResults(String title, List<List<Integer>> numbers) {
        logger.log(Level.INFO, title);
        // System.out.println(title);
        numbers.forEach(values -> {
            logger.log(Level.INFO, values.stream()
                    .sorted(Comparator.naturalOrder())
                    .map(String::valueOf)
                    .collect(Collectors.joining(", " ))
            );
            /*      System.out.println(values.stream()
                    .sorted(Comparator.naturalOrder())
                    .map(String::valueOf)
                    .collect(Collectors.joining(", " ))
            );*/
        });
    }
}
