package app.lotto.lotto;

import lombok.Getter;

@Getter
public enum LottoGameConfig {
        BIG(6, 49),
        MINI(5, 42);

        private final int count;
        private final int bound;

        LottoGameConfig(int count, int bound) {
                this.count = count;
                this.bound = bound;
        }
}
