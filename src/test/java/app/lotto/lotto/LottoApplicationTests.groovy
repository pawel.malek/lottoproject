package app.lotto.lotto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import spock.lang.Specification;

@SpringBootTest
class LottoApplicationTests extends Specification {

    @Autowired(required = false)
    private LottoGenerator lottoGenerator

    @Autowired(required = false)
    private LottoDataManager lottoDataManager;

    def "when context is loaded then all expected beans are created"() {
        expect: "the LottoGenerator is created"
        lottoGenerator
        lottoDataManager
    }

}
