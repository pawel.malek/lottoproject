package app.lotto.lotto

import app.lotto.lotto.model.entity.LottoResultsData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class LottoGeneratorTests extends Specification {

    @Autowired(required = false)
    private LottoGenerator lottoGenerator

    def "isNumberExists should return true if numbers exists in list"() {
        when: "We provide existing numbers"
        then: "Should return true"
        expect: "true"
            Boolean.TRUE == (lottoGenerator.isNumbersExists(availableNumbers, existingNumbers))

        where:
            existingNumbers  | availableNumbers
            Arrays.asList(1, 3, 5, 7, 9, 11) |
            Collections.singletonList(new LottoResultsData("1. 01.01.2000 1,3,5,7,9,11"))
    }

    def "isNumberExists should return false if numbers not exists in list"() {
        when: "We provide not existing numbers"
        then: "Should return false"
        expect: "false"
            Boolean.FALSE == lottoGenerator.isNumbersExists(availableNumbers, notExistingNumbers)

        where:
            notExistingNumbers | availableNumbers
            Arrays.asList(1, 2, 3, 4, 5, 6) |
            Collections.singletonList(new LottoResultsData("1. 01.01.2000 1,3,5,7,9,11"))
    }
}
